CC=gcc
CFLAGS=-Wall -Wextra -Werror -pedantic -O2 -pthread
CFILES=$(shell ls *.c)
PROGS=$(CFILES:%.c=%)

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f $(PROGS)
